$(document).ready(function() {
	
	var showAlert = function(msgData, cssClass) {
		$(".masalert").remove();
		var str = '<div id="alertMsg" class="masalert alert alert-' + cssClass + '"><a class="close" data-dismiss="alert">X</a>' + msgData + '</div>';
		$("#page-wrapper").prepend(str);
		$('body').scrollTo('#alertMsg', 1000, {offset:-60});
	};
	
	$.fn.setDeviceModelOpts = function(data) {
        var self = this, str;
        self.empty();
        if(typeof data == 'object' && data.length > 0) {
            $.each(data, function(i, d) {
                str = '<option value="' + d.id + '">' + d.model_name + '</option>';
                $(str).appendTo(self);
            });
        }
        return self;
    };
    $.fn.setCompanyOpts = function(data) {
        var self = this, str;
        self.empty();
        if(typeof data == 'object' && data.length > 0) {
            $.each(data, function(i, d) {
                str = '<option value="' + d.id + '">' + d.name + '</option>';
                $(str).appendTo(self);
            });
        }
        return self;
    };
    $.fn.setLocationOpts = function(data) {
        var self = this, str;
        self.empty();
        if(typeof data == 'object' && data.length > 0) {
            $.each(data, function(i, d) {
                str = '<option value="' + d.id + '">' + d.location_name + '</option>';
                $(str).appendTo(self);
            });
        }
        return self;
    };
    $.fn.setLabelOpts = function(data) {
        var self = this, str;
        self.empty();
        $('<option value="" class="dim">Choose Status</option>').appendTo(self);
        if(typeof data == 'object' && data.length > 0) {
            $.each(data, function(i, d) {
                str = '<option value="' + d.id + '">' + d.name + '</option>';
                $(str).appendTo(self);
            });
        }
        return self;
    };
    $.fn.setAssignToOpts = function(data) {
        var self = this, str;
        self.empty();
        $('<option value="" class="dim">Choose Assign To</option>').appendTo(self);
        if(typeof data == 'object' && data.length > 0) {
            $.each(data, function(i, d) {
                str = '<option value="' + d + '">' + d + '</option>';
                $(str).appendTo(self);
            });
        }
        return self;
    };
    $.fn.setUserOpts = function(data) {
        var self = this, str;
        self.empty();
        $('<option value="" class="dim">Choose User</option>').appendTo(self);
        if(typeof data == 'object' && data.length > 0) {
            $.each(data, function(i, d) {
                str = '<option value="' + d.id + '">' + d.name + '</option>';
                $(str).appendTo(self);
            });
        }
        return self;
    };
    $.fn.setDepartmentOpts = function(data) {
        var self = this, str;
        self.empty();
        $('<option value="" class="dim">Choose Department</option>').appendTo(self);
        if(typeof data == 'object' && data.length > 0) {
            $.each(data, function(i, d) {
                str = '<option value="' + d.id + '">' + d.name + '</option>';
                $(str).appendTo(self);
            });
        }
        return self;
    };
    
    
    var addDeviceForm = $('#addDeviceForm'),
    device_tag = $('#device_tag'),
    model_id = $('#model_id'),
    device_name = $('#device_name'),
    serial = $('#serial'),
    company_id = $('#company_id'),
    location_id = $('#location_id'),
    current_status = $('#current_status'),
    assign_to = $('#assign_to'),
    department_id = $('#department_id'),
    purchase_date = $('#purchase_date'),
    purchase_cost = $('#purchase_cost'),
    order_number = $('#order_number'),
    user_id = $('#user_id'),
    expected_checkin = $('#expected_checkin'),
    warrenty_months = $('#warrenty_months'),
    is_requestable = $('#is_requestable'),
    form_submit = $('#form_submit');
    
    var assign_to_div = $('#assign_to_div'),
    department_div = $('#department_div'),
    user_div = $('#user_div');
    
    model_id.setDeviceModelOpts(window.deviceModels).selectpicker();
    company_id.setCompanyOpts(window.companies).selectpicker();
    location_id.setLocationOpts(window.locations).selectpicker();
    current_status.setLabelOpts(window.labels).selectpicker();
    assign_to.setAssignToOpts(window.assignToOpts).selectpicker();
    
    assign_to_div.hide();
    department_div.hide();
    user_div.hide();
    
    var reset_assignto = function() {
    	assign_to_div.hide();
        department_div.hide();
        user_div.hide();
        assign_to.val('').selectpicker('refresh');
        department_id.val('').selectpicker('refresh');
        user_id.val('').selectpicker('refresh');
    };
    
    company_id.on('changed.bs.select', function(e, val) {
    	current_status.val('').selectpicker('refresh');
    	reset_assignto();
    });
    location_id.on('changed.bs.select', function(e, val) {
    	current_status.val('').selectpicker('refresh');
    	reset_assignto();
    });
    
    current_status.on('changed.bs.select', function(e, val) {
    	var value = current_status.val();
    	if(value == "") {
    		assign_to_div.hide();
    	    department_div.hide();
    	    user_div.hide();
    	    return;
    	}
    	var url = url_checkDepolyable + "/" + value;
        var http = $.get(url);
        http.done(function(data) {
            if(typeof data == 'object' && data.result == true) {
            	assign_to_div.show();
            	department_div.hide();
        	    user_div.hide();
            }
            else {
            	assign_to_div.hide();
            	department_div.hide();
        	    user_div.hide();
            }
        });
        http.fail(function(){
            alert('Something went wrong. Try again');
        });
    });
    
    assign_to.on('changed.bs.select', function(e, val) {
    	var value = assign_to.val();
    	department_div.hide();
	    user_div.hide();
    	if(value == "") {
    	    return;
    	}
    	var url = url_getAssignTo;
    	var data = {
    		"_token" : window.postToken,
    		"company_id" : company_id.val(),
    		"location_id" : location_id.val(),
    		"assign_to" : value
    	};
        var http = $.post(url, data);
        http.done(function(data) {
            if(typeof data == 'object' && data.result == true) {
            	if(value == "User") {
            		user_id.setUserOpts(data.users).selectpicker('refresh');
            		user_div.show();
            		if( data.users.length < 1 ) {
            			alert("No user(s) found for chosen company");
            		}
            	}
            	else if(value == "Department") {
            		department_id.setDepartmentOpts(data.departments).selectpicker('refresh');
            		department_div.show();
            		if( data.departments.length < 1 ) {
            			alert("No department(s) found for chosen location");
            		}
            	}
            }
        });
        http.fail(function(){
            alert('Something went wrong. Try again');
        });
    });
    
    addDeviceForm.on('submit', function(e) {
    	e.preventDefault();
    	var data = addDeviceForm.serialize();
    	var http = $.post(url_addDevice, data);
        http.done(function(data) {
            if(typeof data == 'object') {
            	showAlert(data.msg, data.result);
            }
            else {
            	showAlert('Something went wrong. Try again', 'danger');
            }
        });
        http.fail(function(){
        	showAlert('Network connection got problem. Try again.', 'warning');
        });
    });
    
});