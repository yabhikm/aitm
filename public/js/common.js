var showAlert = function(msgData, cssClass) {
    $(".masalert").remove();
    var str = '<div id="alertMsg" class="masalert alert alert-' + cssClass + '"><a class="close" data-dismiss="alert">X</a>' + msgData + '</div>';
    $("#page-wrapper").prepend(str);
    $('body').scrollTo('#alertMsg', 1000, {offset:-60});
};