@extends('layouts.layout1')
@section('title', 'Edit Manufacture')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Manufacture</h1>
        <div class="table-responsive">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Status Label</label>
                    <input type="text" name="name" class="form-control" placeholder="Label Name" value="{{ $label->name }}">
                </div>
                <div class="form-group">
                    <label for="name">Status Type</label>
                    <select name="status_type" class="form-control">
                        @foreach($objLabel->statusTypes as $st)
                        <option value="{{ $st }}" @if($label->status_type == $st) selected @endif>{{ $st }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Notes</label>
                    <textarea name="notes" class="form-control" placeholder="Notes">{{ $label->notes }}</textarea>
                </div>
                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection