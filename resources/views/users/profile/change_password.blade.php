@extends('layouts.layout1')
@section('title', 'Change Password')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Change Password</h1>
        <div class="table-responsive">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Current Password</label>
                    <input type="password" name="current_password" class="form-control" placeholder="Enter Current Password" />
                </div>

                <div class="form-group">
                    <label for="name">New Password</label>
                    <input type="password" name="new_password" class="form-control" placeholder="Enter New Password" />
                </div>

                <div class="form-group">
                    <label for="name">Confirm Password</label>
                    <input type="password" name="confirm_password" class="form-control" placeholder="Enter New Password Again" />
                </div>

                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection