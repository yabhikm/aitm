@extends('layouts.layout1')
@section('title', 'Add User')

@section('content')
<div class="innerLR">
	<div class="row">
	    <div class="col-lg-12">
	    	<div class="widget widget-heading-simple widget-body-white">
	    		<div class="widget-head">
					<h1 class="heading">Add User</h1>
				</div>
		        <div class="widget-body">
		        	<div class="row speech-wrap" id="speech_wrap">
						<div class="col-md-9"></div>
					    <div class="col-md-3">
					    	<div class="row">
					        	<div class="col-md-3">
					                <button class="tipsy" id="start_button" onclick="activateSpeech(event)" original-title="Click for speech" style="display: inline-block;">
					                    <img src="{!! CommonHelper::asset('imgs/speech/mic.gif') !!}" alt="start" id="start_img">
					                </button>
					            </div>
					            <div class="col-md-9" id="div_language">
					                <select id="select_language" onchange="updateCountry()"></select>
					                &nbsp;&nbsp;
					                <select id="select_dialect"></select>
					            </div>
					        </div>
					    </div>
					</div>
		            <form method="post">
		                {{ csrf_field() }}
		                <div class="innerB">
		                    <input type="text" name="firstname" class="form-control speech-fill" placeholder="First Name" />
		                </div>
		
		                <div class="innerB">
		                    <input type="text" name="lastname" class="form-control" placeholder="Last Name" />
		                </div>
		
		                <div class="innerB">
		                    <input type="text" name="email" class="form-control" placeholder="Email" />
		                </div>
		
		                <div class="innerB">
		                    <input type="text" name="username" class="form-control" placeholder="Username" />
		                </div>
		
		                <div class="innerB">
		                    <input type="password" name="password" class="form-control" placeholder="Password" />
		                </div>
		                    <select name="usertype" class="selectpicker col-md-6" data-style="btn-primary">
		                    	<option value="">User Type</option>
		                        @foreach($User->user_types as $user_type)
		                        <option value="{{ $user_type }}">{{ $user_type }}</option>
		                        @endforeach
		                    </select>
		
		                <div class="innerB">	
		                    <input type="text" name="job_title" class="form-control" placeholder="Job Title" />
		                </div>
		
		                <div class="innerB">
		                    <input type="text" name="mobile" class="form-control" placeholder="Mobile" />
		                </div>
		
		                <div class="innerB">
		                    <input type="text" name="emp_code" class="form-control" placeholder="Emp. Code" />
		                </div>
		
		
		                <div class="innerB">
		                    <select name="company_id" class="selectpicker col-md-6" data-style="btn-primary">
		                        @foreach($Company->getCompanies() as $com)
		                        <option value="{{ $com->id }}">{{ $com->name }}</option>
		                        @endforeach
		                    </select>
		                </div>
		
		                <div class="innerB">
		                    <select name="location_id" class="selectpicker col-md-6" data-style="btn-primary">
		                        @foreach($Location->getLocations() as $loc)
		                        <option value="{{ $loc->id }}">{{ $loc->name }}</option>
		                        @endforeach
		                    </select>
		                </div>
		
		                <div class="innerB">
		                    <select name="department_id" class="selectpicker col-md-6" data-style="btn-primary">
		                        @foreach($Department->getDepartments() as $dep)
		                        <option value="{{ $dep->id }}">{{ $dep->name }}</option>
		                        @endforeach
		                    </select>
		                </div>
		
		                <div class="innerB">
		                    <textarea name="notes" class="form-control"></textarea>
		                </div>
		
		                <button type="submit" class="btn btn-primary">Save</button>
		            </form>
		        </div>
			</div>		        
	    </div>
	</div>
</div>
@endsection

@push('lib')
    <script src="{!! CommonHelper::asset('js/speech.js') !!}"></script>
@endpush