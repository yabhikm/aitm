@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Create Ticket
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
	{!! Html::style('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') !!}
    <!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1 class="title-capitalize">Create Ticket</h1>

        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">Ticket</li>
        </ol>
    </section>
    <section class="content">
        {{-- BOLLATI --}}
        <div class="row" style="margin-top: 20px">
            <div class="col-md-12">
                @beginbox(['title' => 'Create', 'titleClass' => 'panel-primary', 'id' => 'analyticMarginBox','iconTitle' => 'fa-files-o', 'enableCollapse' => false, 'enableClose' => false])
                <form method="POST" action="" role="form" id="createTicket" class="form-horizontal" onsubmit="createTicket()">
	                <div class="form-group">
						<label for="ticket_title" class="col-md-3 control-label">Request Title : </label>
					    <div class="input-group col-md-4">
					        <input placeholder="Title" class="form-control ticket_title" name="ticket_title" id="ticket_title" type="text" required>
					    </div>
					</div>
	                <div class="form-group">
						<label for="ticket_type" class="col-md-3 control-label">Request Type : </label>
					    <div class="input-group col-md-4">
					    	<select class="ticket_type form-control" id="ticket_type" name="ticket_type" required>
					    		<option value="Type 1">Type 1</option>
					    		<option value="Type 2">Type 2</option>
					    		<option value="Type 3">Type 3</option>
					    		<option value="Type 4">Type 4</option>
					    	</select>
					    </div>
					</div>
					<div class="form-group">
						<label for="analytic_end" class="col-md-3 control-label">Description : </label>
					    <div class="input-group col-md-4">
					    	<textarea rows="" cols="" name="description" class="description form-control" required></textarea>
					    </div>
					</div>
					
					<div class="form-group">
            			<label for="expected_date" class="control-label col-md-3">Expected Date : </label>
            			<div data-date-start-date="+0d" data-date-format="dd-mm-yyyy" class="input-group date expected_date col-md-4">	
							<span class="input-group-addon showCalender">
                				<i class="fa fa-calendar"></i>
            				</span>	
			                <input id="expected_date" name="expected_date" data-fieldtype="timestamp" data-typecast="date" readonly="" class="datepicker form-control expected_date" value="" type="date" required>
						</div>
					</div>
					
					<div class="form-group">
						<div class="form-actions">
						 	<div class="col-md-3"></div>
							<div class="col-md-4">
								<button class="btn blue" type="submit">Submit</button>
							</div>
						</div>
					</div>
					{{ csrf_field() }}
	                </form>
                @closebox
            </div>
        </div>
    </section>
@stop

@section('footer_scripts')
    {{-- Data Tables --}}
    {!! Html::script('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}
    {!! Html::script('assets/js/pages/ticket.js') !!}
@stop

