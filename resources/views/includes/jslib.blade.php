<script type="text/javascript" src="{!! CommonHelper::asset('plugins/selectpicker/js/bootstrap-select.min.js') !!}"></script>
<script type="text/javascript" src="{!! CommonHelper::asset('plugins/datatable/js/jquery.dataTables.min.js') !!}"></script>
<script type="text/javascript" src="{!! CommonHelper::asset('plugins/datatable/js/dataTables.bootstrap.min.js') !!}"></script>
<script type="text/javascript" src="{!! CommonHelper::asset('plugins/moment/moment.min.js') !!}"></script>
<script type="text/javascript" src="{!! CommonHelper::asset('plugins/bsdatetimepicker/js/bootstrap-datetimepicker.min.js') !!}"></script>