<div id="menu" class="hidden-print hidden-xs sidebar-default sidebar-brand-primary">
	<div id="sidebar-social-wrapper">
		<div id="brandWrapper">
			<a href="{{ url('dashboard') }}"><span class="text">Dashboard</span></a>
		</div>
		<ul class="menu list-unstyled">			
			<li class="">
				<a href="{{ url('users') }}">
					<i class="fa fa-users"></i>
					<span>Users</span>
				</a>
			</li>
			
			<li class="">
				<a href="{{ url('devices') }}">
					<i class="fa fa-mobile"></i>
					<span>Devices</span>
				</a>
			</li>
			
			<li class="">
				<a href="{{ url('accessories') }}">
					<i class="fa fa-cog"></i>
					<span>Accessories</span>
				</a>
			</li>
			
			<li class="">
				<a href="{{ url('components') }}">
					<i class="fa fa-cog"></i>
					<span>Components</span>
				</a>
			</li>
			
			<li class="">
				<a href="{{ url('licenses') }}">
					<i class="fa fa-certificate"></i>
					<span>Licenses</span>
				</a>
			</li>
		</ul>
	</div>					
</div>





			
		</div>