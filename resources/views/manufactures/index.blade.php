@extends('layouts.layout1')
@section('title', 'Manufatures')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Manufactures</span>
            &nbsp;
            <a href="{{ url('manufacture/add') }}" class="link">Add</a>
        </h1>
        <div class="table-responsive">
            <table class="mytable table table-striped">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Manufacture Name</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($manufacture->getManufactures() as $manu)
                    <tr>
                        <td>
                            <a href="{{ url('manufacture/edit') }}/{{ $manu->id }}" data-id="{{ $manu->id }}" >Edit</a>
                        </td>
                        <td>{{ $manu->name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection