@extends('layouts.layout1')
@section('title', 'Purchases')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Purchases</span>
            &nbsp;
            <a href="{{ url('purchase/add') }}" class="link">Add</a>
        </h1>
        <div class="table-responsive">
            <table id="mytable" class="mytable table table-striped">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Invoice Number</th>
                        <th>Purchase Date</th>
                        <th>Company</th>
                        <th>Supplier</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Country</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('head')
@include('includes.csslib')
@endpush

@push('lib')
@include('includes.jslib')
<script type="text/javascript">
    var config = new Object;
    config.url = new Object;
    config.url.purchases = "{{ url('jx-purchases') }}";
    config.url.edit = "{{ url('purchase/edit') }}";
    config.token = "{{ csrf_token() }}";
</script>
<script type="text/javascript" src="{!! CommonHelper::asset('js/purchase/index.js') !!}"></script>
@endpush