@extends('layouts.layout1')
@section('title', 'Add Device')

<?php
$deviceModels = $model->getDeviceModels();
$companies = $company->getCompanies(array(
    "id", "name"
        ), "name");
$locations = $location->getLocationsForOpts();
$labels = $label->getLabelsNotIn();
?>

@section('content')
<div class="innerLR">
	<div class="row" data-ng-app="itm" data-ng-controller="deviceCtrl" data-ng-init="">
	    <div class="col-lg-12">
	    	<div class="widget widget-heading-simple widget-body-white">
	        	<div class="widget-head">
					<h1 class="heading">Add Device</h1>
				</div>
		        <div class="widget-body">
		            <form id="addDeviceForm" method="post" class="form-horizontal">
		                {{ csrf_field()}}		
		                
	                    <div class="innerB">
	                        <input type="text" name="device_tag" id="device_tag" class="form-control" placeholder="Enter Device Tag">
	                    </div>
						
						<select name="model_id" id="model_id" class="form-control selectpicker col-md-6" data-style="btn-primary" data-live-search="true"></select>
						
	                    <div class="innerB">
	                        <input type="text" name="device_name" id="device_name" class="form-control" placeholder="Enter Device Name">
	                    </div>
	                    
		                <div class="innerB">
		                	<input type="text" name="serial" id="serial" class="form-control" placeholder="Enter Serial">
						</div>
						
						<div class="innerB">
                        	<select name="company_id" id="company_id" class="form-control selectpicker col-md-6" data-style="btn-primary" data-live-search="true"></select>
						</div>
						
						<div class="innerB">
                        	<select name="location_id" id="location_id" class="form-control selectpicker col-md-6" data-style="btn-primary" data-live-search="true"></select>
						</div>
						
						<div class="innerB">
                        	<select name="current_status" id="current_status" class="form-control selectpicker col-md-6" data-style="btn-primary" data-live-search="true"></select>
	                    </div>
	                    
	                    <div class="innerB">
	                 		<select name="assign_to" id="assign_to" class="form-control selectpicker col-md-6" data-style="btn-primary"></select>
	                    </div>
	                    
	                    <div class="innerB">
	                    	<select name="department_id" id="department_id" class="form-control selectpicker" data-live-search="true"></select>
						</div>
						
						<div class="innerB">
							<select name="user_id" id="user_id" class="form-control selectpicker" data-live-search="true"></select>
		                </div>
		                
                		<div class="innerB">
		                	<input type="text" name="expected_checkin" id="expected_checkin" class="form-control" placeholder="Choose Expected Return Date">
		                </div>

						<div class="innerB">
		                	<input type="text" name="purchase_date" id="purchase_date" class="form-control" placeholder="Enter Purchase Date">
						</div>

	                    <div class="innerB">
	                        <input type="text" name="purchase_cost" id="purchase_cost" class="form-control" placeholder="Enter Purchase Cost">
	                    </div>

	                    <div class="innerB">
	                        <input type="text" name="order_number" id="order_number" class="form-control" placeholder="Enter Order Number">
	                    </div>

	                    <div class="innerB">
	                        <input type="text" name="warrenty_months" id="warrenty_months" class="form-control" />
	                    </div>
	                    
	                    <div class="innerB">
	                        <select name="is_requestable" id="is_requestable" class="form-control selectpicker">
	                            <option value="0">No</option>
	                            <option value="1">Yes</option>
	                        </select>
						</div>

	                    <div class="innerB">
	                        <textarea name="last_remarks" id="last_remarks" class="form-control textarea minTextArea"></textarea>
	                    </div>

	                    <div class="innerB">
	                        <button type="submit" id=form_submit" class="btn btn-default">Save</button>
	                    </div>
		            </form>
		        </div>
			</div>		       
	    </div>
	</div>
</div>
@endsection

@push('head')
@include('includes.csslib')
@endpush

@push('lib')
@include('includes.jslib')
<script type="text/javascript">
    var postToken = "{{ csrf_token() }}";
    var deviceModels = {!! json_encode($deviceModels) !!};
    var companies = {!! json_encode($companies) !!};
    var locations = {!! json_encode($locations) !!};
    var labels = {!! json_encode($labels) !!};
    var assignToOpts = {!! json_encode(array("User", "Department")) !!};
    var url_checkDepolyable = "{{ url('check-deployable') }}";
    var url_getAssignTo = "{{ url('device/get-assign-to') }}";
    var url_addDevice = "{{ url('device/add-device') }}";</script>
<script type="text/javascript" src="{!! CommonHelper::asset('js/devices/add.js')  !!}"></script>
@endpush