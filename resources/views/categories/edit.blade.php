@extends('layouts.layout1')
@section('title', 'Edit Category')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Category</h1>
        <div class="table-responsive">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Category Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Category Name" value="{{ $category->name }}" />
                </div>
                <div class="form-group">
                    <label for="name">Category Type</label>
                    <select name="category_type" class="form-control">
                        @foreach($objCategory->category_types as $ct)
                        <option value="{{ $ct }}" @if($ct == $category->category_type) selected @endif >{{ $ct }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection