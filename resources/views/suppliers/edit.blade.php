@extends('layouts.layout1')
@section('title', 'Edit Supplier')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Supplier</h1>
        <form method="post">
            {{ csrf_field() }}

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="name" class="control-label">Supplier Name</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Enter Supplier Name" value="{{ $supplier->name }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="contact" class="control-label">Contact Person Name</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="contact" id="contact" class="form-control" placeholder="Enter Contact Person Name" value="{{ $supplier->contact }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="address" class="control-label">Street 1</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="address" id="address" class="form-control" placeholder="Enter Street 1" value="{{ $supplier->address }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="address2" class="control-label">Street 2</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="address2" id="address2" class="form-control" placeholder="Enter Street 2" value="{{ $supplier->address2 }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="city" class="control-label">City</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="city" id="city" class="form-control" placeholder="Enter City" value="{{ $supplier->city }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="state" class="control-label">State</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="state" id="state" class="form-control" placeholder="Enter State" value="{{ $supplier->state }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="country" class="control-label">Country</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <select name="country" id="country" class="form-control">
                        @foreach($objSupplier->getCountries() as $country)
                        <option value="{{ $country->country_code }}" @if($supplier->country == $country->country_code) selected @endif >{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="zip" class="control-label">Zip/Postal Code</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="zip" id="zip" class="form-control" placeholder="Zip/Postal Code" value="{{ $supplier->zip }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="phone" class="control-label">Phone</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Enter Phone" value="{{ $supplier->phone }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="email" class="control-label">Email Address</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email Address" value="{{ $supplier->email }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="url" class="control-label">Website URL</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="url" id="url" class="form-control" placeholder="Enter Website URL" value="{{ $supplier->url }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="notes" class="control-label">Notes</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <textarea name="notes" id="notes" class="form-control minTextArea">{{ $supplier->notes }}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    &nbsp;
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <button type="submit" id="form_submit" class="btn btn-default">Save</button>
                </div>
            </div>

        </form>
    </div>
</div>
@endsection