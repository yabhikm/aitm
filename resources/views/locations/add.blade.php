@extends('layouts.layout1')
@section('title', 'Add Location')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Location</h1>
        <div class="table-responsive">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Location Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Location Name">
                </div>
                
                <div class="form-group">
                    <label for="company_id" class="control-label">Company</label>
                    <select name="company_id" id="company_id" class="form-control">
                        @foreach($company->getCompanies(array("id", "name"), "name") as $comp)
                        <option value="{{ $comp->id }}">{{ $comp->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Street 1</label>
                    <input type="text" name="street1" class="form-control" placeholder="Street 1">
                </div>

                <div class="form-group">
                    <label for="name">Street 2</label>
                    <input type="text" name="street2" class="form-control" placeholder="Street 2">
                </div>

                <div class="form-group">
                    <label for="name">City</label>
                    <input type="text" name="city" class="form-control" placeholder="City">
                </div>

                <div class="form-group">
                    <label for="name">State</label>
                    <input type="text" name="state" class="form-control" placeholder="State">
                </div>

                <div class="form-group">
                    <label for="name">Country</label>
                    <select name="country" class="form-control" >
                        @foreach($objLocation->getCountries() as $country)
                        <option value="{{ $country->country_code }}">{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Zip/Postal Code</label>
                    <input type="text" name="zip" class="form-control" placeholder="Zip/Postal Code">
                </div>
                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection