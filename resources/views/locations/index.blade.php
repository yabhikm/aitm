@extends('layouts.layout1')
@section('title', 'Locations')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Locations</span>
            &nbsp;
            <a href="{{ url('location/add') }}" class="link">Add</a>
        </h1>
        <div class="table-responsive">
            <table class="mytable table table-striped">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Location Name</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Zip</th>
                        <th>Country</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objLocation->getLocations() as $loc)
                    <tr>
                        <td>
                            <a href="{{ url('location/edit') }}/{{ $loc->id }}" data-id="{{ $loc->id }}" >Edit</a>
                        </td>
                        <td>{{ $loc->name }}</td>
                        <td>{{ $loc->street1 . " " . $loc->street2 }}</td>
                        <td>{{ $loc->city }}</td>
                        <td>{{ $loc->state }}</td>
                        <td>{{ $loc->zip }}</td>
                        <td>{{ $loc->country_name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection