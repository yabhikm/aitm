@extends('layouts.layout1')
@section('title', 'Add Company')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Company</h1>
        <div class="table-responsive">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Company Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Company Name">
                </div>
                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection