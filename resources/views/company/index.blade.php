@extends('layouts.layout1')
@section('title', 'Company')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Company</span>
            &nbsp;
            <a href="{{ url('company/add') }}" class="link">Add</a>
        </h1>
        <div class="table-responsive">
            <table id="mytable" class="mytable table table-striped">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Company Name</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('head')
@include('includes.csslib')
@endpush

@push('lib')
@include('includes.jslib')
<script type="text/javascript">
    var config = new Object;
    config.url = new Object;
    config.url.companies = "{{ url('jx-companies') }}";
    config.url.edit = "{{ url('company/edit') }}";
    config.token = "{{ csrf_token() }}";
</script>
<script type="text/javascript" src="{!! CommonHelper::asset('js/company/index.js') !!}"></script>
@endpush