<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::group(['middleware' => 'web'], function() {

    Route::any('login', 'Auth\AuthController@login');
    Route::any('forgot-password', 'Auth\CustomPasswordController@reset');
    Route::post('jx-pass-reset', 'Auth\CustomPasswordController@sendResetLinkEmail');
    Route::any('password-reset/{token?}/{id?}', 'Auth\CustomPasswordController@showResetForm');

    Route::group(['middleware' => ['auth', 'revalidate']], function() {

        Route::get('companies', 'CompanyController@getIndex');
        Route::any('company/add', 'CompanyController@addCompany');
        Route::any('company/edit/{id}', 'CompanyController@editCompany');
        Route::post('jx-companies', 'CompanyController@ajaxCompanies');

        Route::get('manufactures', 'ManufactureController@getIndex');
        Route::any('manufacture/add', 'ManufactureController@addManufacture');
        Route::any('manufacture/edit/{id}', 'ManufactureController@editManufacture');

        Route::get('models', 'ModelController@getIndex');
        Route::any('model/add', 'ModelController@addModel');
        Route::any('model/edit/{id}', 'ModelController@editModel');

        Route::get('categories', 'CategoryController@getIndex');
        Route::any('category/add', 'CategoryController@addCategory');
        Route::any('category/edit/{id}', 'CategoryController@editCategory');

        Route::get('status-labels', 'LabelController@getIndex');
        Route::any('status-label/add', 'LabelController@addLabel');
        Route::any('status-label/edit/{id}', 'LabelController@editLabel');
        Route::get('check-deployable/{id}', 'LabelController@checkDeployable');

        Route::get('locations', 'LocationController@getIndex');
        Route::any('location/add', 'LocationController@addLocation');
        Route::any('location/edit/{id}', 'LocationController@editLocation');
        
        Route::get('suppliers', 'SupplierController@getIndex');
        Route::any('supplier/add', 'SupplierController@addSupplier');
        Route::any('supplier/edit/{id}', 'SupplierController@editSupplier');
        
        Route::get('purchases', 'PurchaseController@getIndex');
        Route::any('purchase/add', 'PurchaseController@addPurchase');
        Route::any('purchase/edit/{id}', 'PurchaseController@editPurchase');
        Route::post('jx-purchases', 'PurchaseController@ajaxPurchases');

        Route::get('departments', 'DepartmentController@getIndex');
        Route::any('department/add', 'DepartmentController@addDepartment');
        Route::any('department/edit/{id}', 'DepartmentController@editDepartment');

        Route::get('users', 'UserController@getIndex');
        Route::any('user/add', 'UserController@addUser');
        Route::any('user/edit/{id}', 'UserController@editUser');

        Route::get('profile', 'ProfileController@getIndex');
        Route::any('profile/edit', 'ProfileController@editProfile');
        Route::any('profile/change-password', 'ProfileController@changePassword');

        Route::get('devices', 'DeviceController@getIndex');
        Route::any('device/add', 'DeviceController@addDevice');
        Route::post('device/add-device', 'DeviceController@ajaxAddDevice');
        Route::post('device/get-assign-to', 'DeviceController@getAssignTo');

        Route::get('dashboard', ['as' => 'admin.dashboard', 'uses' => 'HomeController@dashboard']);
        Route::get('logout', 'Auth\AuthController@logout');
    });

    Route::get('/', function () {
        if (Auth::check()) {
            return redirect()->action('HomeController@dashboard');
        } else {
            return redirect()->to('login');
        }
    });
    
	Route::group(['prefix' => 'uticket'], function () {
    	Route::get('list', [
    		'as' => 'admin.uticket.list',
    		'uses' => 'UTicketController@ticketList',
		]);
    		 
    	Route::get('add', [
    		'as' => 'admin.uticket.add',
    		'uses' => 'UTicketController@add',
		]);
    		 
    	Route::post('add', [
    		'as' => 'admin.uticket.add',
    		'uses' => 'UTicketController@addPost',
		]);
    	
		Route::get('filterData', [
    		'as' => 'admin.uticket.filterData',
    		'uses' => 'UTicketController@filterData'
		]);
    	
    	Route::get('view/{id}', [
    		'as' => 'admin.uticket.view',
    		'uses' => 'UTicketController@view'
		]);
    		 
    	Route::post('close/{id}', [
    		'as' => 'admin.uticket.close',
    		'uses' => 'UTicketController@close'
    	]);
	});
    	
    Route::group(['prefix' => 'iticket'], function () {
    	Route::get('list', [
    		'as' => 'admin.iticket.list',
    		'uses' => 'ITicketController@ticketList',
		]);
    	
    	Route::get('filterData', [
    		'as' => 'admin.iticket.filterData',
    		'uses' => 'ITicketController@filterData'
		]);
    			 
    	Route::get('view/{id}', [
    		'as' => 'admin.iticket.view',
    		'uses' => 'ITicketController@view'
		]);
    			 
    	Route::post('fix/{id}', [
    		'as' => 'admin.iticket.fix',
    		'uses' => 'ITicketController@fix'
		]);
	});
});
