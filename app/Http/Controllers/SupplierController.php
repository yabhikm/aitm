<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Supplier;
use Auth;

class SupplierController extends Controller {

    public function getIndex() {
        return view("suppliers.index")->with("objSupplier", new Supplier());
    }

    public function addSupplier(Request $request) {
        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given supplier"
            );
            $data = $request->only("name", "contact", "address", "address2", "city", "state", "country", "zip", "phone", "email", "url", "notes");
            $data["user_id"] = Auth::user()->id;
            $objSupplier = new Supplier;
            if ($objSupplier->addSupplier($data)) {
                $return["msg"] = "Supplier added successfully";
                $return["status"] = "success";
            }
            $request->session()->flash("msg", $return);
        }
        return view("suppliers.add")->with("objSupplier", new Supplier());
    }

    public function editSupplier($id, Request $request) {
        $objSupplier = new Supplier;
        $get_supplier = $objSupplier->getSupplierById($id);

        if (count($get_supplier) != 1) {
            return redirect()->action("SupplierController@getIndex")->with("msg", array(
                "status" => "warning",
                "msg" => "No Supplier found for given data"
            ));
        }

        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to edit given Supplier"
            );

            $data = $request->only("name", "contact", "address", "address2", "city", "state", "country", "zip", "phone", "email", "url", "notes");
            if ($objSupplier->updateSupplierById($data, $id)) {
                $return["msg"] = "Supplier updated successfully";
                $return["status"] = "success";
                return redirect()->action("SupplierController@getIndex")->with("msg", $return);
            }

            $request->session()->flash("msg", $return);
        }

        return view("Suppliers.edit")->with("objSupplier", new Supplier())
                ->with("supplier", $get_supplier[0]);
    }
}
