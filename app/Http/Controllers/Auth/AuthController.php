<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers,
    ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    protected function login(Request $request) {
        if ($request->method() != 'POST') {
            return view("auth.login");
        }

        $return = array(
            "status" => "danger"
        );

        $username = $request->input("username", "");
        $password = $request->input("password", "");

        $username = trim($username);
        $password = trim($password);

        if (!$username || !$password) {
            $return["msg"] = "Please fill username and password";
            return view("auth.login")->with("msg", $return);
        }

        $email = $username;
        $activated = 1;

        $via_username = compact('username', 'password', 'activated');
        $via_email = compact('email', 'password', 'activated');

        if (Auth::attempt($via_username) || Auth::attempt($via_email)) {
            $return["msg"] = "Welcome, You have logged in successfully!";
            return redirect()->action("HomeController@dashboard")->with("msg", $return);
        }

        $return["msg"] = "Username and Password are invalid";
        return view("auth.login")->with("msg", $return);
    }

}
