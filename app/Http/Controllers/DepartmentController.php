<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Location;

class DepartmentController extends Controller {

    public function getIndex() {
        return view("departments.index")->with("objDepartment", new Department());
    }

    public function addDepartment(Request $request) {
        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given Department"
            );
            $data = $request->only("name", "location_id");
            $objDepartment = new Department;
            if ($objDepartment->addDepartment($data)) {
                $return["msg"] = "Department added successfully";
                $return["status"] = "success";
            }
            $request->session()->flash("msg", $return);
        }
        return view("departments.add")->with("objLocation", new Location());
    }

    public function editDepartment($id, Request $request) {
        $objDepartment = new Department;
        $get_department = $objDepartment->getDepartmentById($id);

        if (count($get_department) != 1) {
            return redirect()->action("DepartmentController@getIndex")->with("msg", array(
                        "status" => "warning",
                        "msg" => "No Department found for given data"
            ));
        }

        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to edit given Department"
            );

            $data = $request->only("name", "location_id");
            if ($objDepartment->updateDepartmentById($data, $id)) {
                $return["msg"] = "Department updated successfully";
                $return["status"] = "success";
                return redirect()->action("DepartmentController@getIndex")->with("msg", $return);
            }

            $request->session()->flash("msg", $return);
        }

        return view("departments.edit")->with("objLocation", new Location())
                        ->with("department", $get_department[0]);
    }

}
