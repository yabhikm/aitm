<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Model;
use App\Models\Category;
use App\Models\Manufacture;

class ModelController extends Controller {

    public function getIndex() {
        return view("models.index")->with("objModel", new Model());
    }

    public function addModel(Request $request) {
        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given Manufacture"
            );
            $data = $request->only("name", "model_no", "manufact_id", "category_id");
            $objModel = new Model;
            if ($objModel->addModel($data)) {
                $return["msg"] = "Model added successfully";
                $return["status"] = "success";
            }
            $request->session()->flash("msg", $return);
        }
        return view("models.add")->with("objCategory", new Category())->with("objManufacture", new Manufacture());
    }

    public function editModel($id, Request $request) {
        $objModel = new Model;
        $get_model = $objModel->getModelById($id);

        if (count($get_model) != 1) {
            return redirect()->action("ModelController@getIndex")->with("msg", array(
                        "status" => "warning",
                        "msg" => "No Model found for given data"
            ));
        }

        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given Model"
            );

            $data = $request->only("name", "model_no", "manufact_id", "category_id");
            if ($objModel->updateModelById($data, $id)) {
                $return["msg"] = "Model updated successfully";
                $return["status"] = "success";
                return redirect()->action("ModelController@getIndex")->with("msg", $return);
            }

            $request->session()->flash("msg", $return);
        }

        return view("models.edit")->with("objCategory", new Category())
                        ->with("objManufacture", new Manufacture())
                        ->with("model", $get_model[0]);
    }

}
