<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Hash;

class ProfileController extends Controller {

    public function getIndex() {
        $User = new User();
        $user = $User->getUserProfile(Auth::user()->id);
        return view("users.profile.index")->with("user", $user[0]);
    }

    public function changePassword(Request $request) {
        if ($request->method() == "POST") {
            $data = $request->only("current_password", "new_password", "confirm_password");
            $current_password = trim($data["current_password"]);
            $new_password = trim($data["new_password"]);
            $confirm_password = trim($data["confirm_password"]);

            if (!$new_password || !$confirm_password || !$current_password) {
                $request->session()->flash("msg", array(
                    "status" => "danger",
                    "msg" => "Please fill all fields properly"
                ));
                return view("users.profile.change_password")->with("input", $data);
            }

            if ($new_password != $confirm_password) {
                $request->session()->flash("msg", array(
                    "status" => "danger",
                    "msg" => "New Password and Confirm Password fields must be match"
                ));
                return view("users.profile.change_password")->with("input", $data);
            }

            $User = new User();
            $getUser = $User->getUserById(Auth::user()->id);

            if (!Hash::check($current_password, $getUser[0]->password)) {
                $request->session()->flash("msg", array(
                    "status" => "danger",
                    "msg" => "Current Password is invalid"
                ));
                return view("users.profile.change_password")->with("input", $data);
            }

            $User->updateUserById(array(
                "password" => $new_password
                    ), Auth::user()->id);

            $request->session()->flash("msg", array(
                "status" => "success",
                "msg" => "New Password has been updated successfully"
            ));
        }

        return view("users.profile.change_password");
    }

}
