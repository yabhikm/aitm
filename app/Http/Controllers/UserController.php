<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Location;
use App\Models\Company;
use App\Models\Department;

class UserController extends Controller {

    public function getIndex() {
        return view("users.index")->with("User", new User);
    }

    public function addUser(Request $request) {
        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given User"
            );
            $data = $request->only("firstname", "lastname", "email", "username", "password", "usertype", "job_title", "mobile", "emp_code", "company_id", "location_id", "department_id", "notes");
            $objUser = new User;
            if ($objUser->addUser($data)) {
                $return["msg"] = "User added successfully";
                $return["status"] = "success";
            }
            $request->session()->flash("msg", $return);
        }
        return view("users.add")
                        ->with('User', new User())
                        ->with('Location', new Location())
                        ->with('Company', new Company())
                        ->with('Department', new Department());
    }

    public function editUser($id, Request $request) {
        $objUser = new User;
        $get_comp = $objUser->getUserById($id);

        if (count($get_comp) != 1) {
            return redirect()->action("UserController@getIndex")->with("msg", array(
                        "status" => "warning",
                        "msg" => "No User found for given data"
            ));
        }

        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given User"
            );

            $data = $request->only("firstname", "lastname", "email", "username", "password", "usertype", "job_title", "mobile", "emp_code", "company_id", "location_id", "department_id", "notes");
            if (!$data["password"]) {
                unset($data["password"]);
            }
            if ($objUser->updateUserById($data, $id)) {
                $return["msg"] = "User updated successfully";
                $return["status"] = "success";
                return redirect()->action("UserController@getIndex")->with("msg", $return);
            }

            $request->session()->flash("msg", $return);
        }

        return view("users.edit")
                        ->with('User', new User())
                        ->with('Location', new Location())
                        ->with('Company', new Company())
                        ->with('Department', new Department())
                        ->with("user", $get_comp[0]);
    }

}
