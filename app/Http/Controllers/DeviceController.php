<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Device;
use App\Models\Model;
use App\Models\Location;
use App\Models\Company;
use App\Models\Label;
use App\Models\Department;
use App\Models\User;
use Validator;
use Auth;

class DeviceController extends Controller {

    public function getIndex() {
        return view("devices.index")->with("Device", new Device);
    }

    public function addDevice(Request $request) {
        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given Device"
            );
            $data = $request->only("name");
            $objDevice = new Device;
            if ($objDevice->addDevice($data)) {
                $return["msg"] = "Device added successfully";
                $return["status"] = "success";
            }
            $request->session()->flash("msg", $return);
        }
        return view("devices.add")
                        ->with('model', new Model())
                        ->with('location', new Location())
                        ->with('company', new Company())
                        ->with('department', new Department())
                        ->with('label', new Label());
    }

    public function editDevice($id, Request $request) {
        $objDevice = new Device;
        $get_comp = $objDevice->getDeviceById($id);

        if (count($get_comp) != 1) {
            return redirect()->action("DeviceController@getIndex")->with("msg", array(
                        "status" => "warning",
                        "msg" => "No Device found for given data"
            ));
        }

        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given Device"
            );

            $data = $request->only("name");
            if ($objDevice->updateDeviceById($data, $id)) {
                $return["msg"] = "Device updated successfully";
                $return["status"] = "success";
                return redirect()->action("DeviceController@getIndex")->with("msg", $return);
            }

            $request->session()->flash("msg", $return);
        }

        return view("devices.edit")->with("Device", $get_comp[0]);
    }

    public function getAssignTo(Request $request) {
        $result = array("result" => false);
        $data = $request->only("company_id", "location_id", "assign_to");
        $company_id = trim($data["company_id"]);
        $location_id = trim($data["location_id"]);
        $assign_to = trim($data["assign_to"]);

        if ($company_id && $location_id && in_array($assign_to, array("User", "Department"))) {
            $result["result"] = true;
            if ($assign_to == "User") {
                $objUser = new User();
                $getUsers = $objUser->getUsersToOpts(array(array("company_id", "=", $company_id)));
                $result["users"] = $getUsers;
            } else {
                $objDepartment = new Department();
                $getDepartments = $objDepartment->getDepartmentOpts(array(
                    array("location_id", "=", $location_id)
                ));
                $result["departments"] = $getDepartments;
            }
        }

        return response()->json($result);
    }

    public function ajaxAddDevice(Request $request) {
        $result = array("result" => "danger");
        $data = $request->except("_token");

        foreach ($data as $k => $v) {
            $data[$k] = trim($v);
        }

        $rules = array(
            "device_tag" => "required|string",
            "model_id" => "required|integer",
            "device_name" => "required",
            "serial" => "string",
            "company_id" => "required|integer",
            "location_id" => "required|integer",
            "current_status" => "required|integer",
            "assign_to" => "string",
            "department_id" => "integer",
            "user_id" => "integer",
            "expected_checkin" => "",
            "purchase_date" => "",
            "purchase_cost" => "",
            "order_number" => "string",
            "warrenty_months" => "integer",
            "is_requestable" => "integer",
            "last_remarks" => "string"
        );

        $objLabel = new Label();
        $get_Label = $objLabel->getLabelById($data["current_status"]);
        $is_deployable = (count($get_Label) && $get_Label[0]->status_type == "Deployable") ? true : false;

        if ($is_deployable) {
            $rules["assign_to"] = "required|string";
            if ($data["assign_to"] == "User") {
                $rules["user_id"] = "required|integer";
            } elseif ($data["assign_to"] == "Department") {
                $rules["department_id"] = "required|integer";
            }
        }

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $msgs = $validator->errors()->all();
            $tmp_msg = '<div>Please correct following values:</div><ul>';
            foreach ($msgs as $k => $msg) {
                $tmp_msg .= '<li>' . $msg . '</li>';
            }
            $tmp_msg .= '</ul>';
            $return['msg'] = $tmp_msg;
            return response()->json($return);
        }

        if ($is_deployable && $data["assign_to"] == "User" && $data["user_id"]) {
            $data['current_status'] = 2; /* Assigned Status */
            $data['last_checkout'] = date('Y-m-d H:i:s');
            $data["department_id"] = "";
        } elseif ($is_deployable && $data["assign_to"] == "Department" && $data["department_id"]) {
            $data['current_status'] = 2; /* Assigned Status */
            $data['last_checkout'] = date('Y-m-d H:i:s');
            $data["user_id"] = "";
        }

        $data['created_by'] = Auth::user()->id;

        $objDevice = new Device;
        $device_id = $objDevice->addDevice($data);
        
        if( $data['current_status'] == 2 ) {
            $log = array(
                'device_id' => $device_id,
                'action_type' => 1,
                'user_id' => $data["user_id"],
                'assign_to' => $data["assign_to"],
                'department_id' => $data['department_id'],
                'logged_by' => $data['created_by'],
                'logged_at' => $data['last_checkout'],
                'log_remarks' => $data['last_remarks']
            );
            $objDevice->addDeviceLog($log);
        }

        $return["result"] = "success";
        $return["msg"] = "New device added successfully";
        return response()->json($return);
    }

}
