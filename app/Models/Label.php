<?php

namespace App\Models;

use App\Models\Base;
use DB;

class Label extends Base {

    public $statusTypes = array("Deployable", "Non Deployable", "Archived", "Deployed");

    public function getLabels() {
        return DB::table($this->tblLabel)->get();
    }

    public function getLabelById($id) {
        return DB::table($this->tblLabel)->where("id", "=", $id)->limit(1)->get();
    }

    public function addLabel($data) {
        $data["created_at"] = $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblLabel)->insert($data);
    }

    public function updateLabelById($data, $id) {
        $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblLabel)->where("id", $id)->update($data);
    }

    public function getLabelsNotIn($values = array("Deployed"), $select = array("id", "name")) {
        return DB::table($this->tblLabel)
                        ->select($select)
                        ->whereNotIn("status_type", $values)
                        ->get();
    }

}
