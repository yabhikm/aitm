<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketDesc extends Model{
   
    protected $table = 'ticket_desc';
    public $timestamps = false;
    protected $primaryKey = 'td_id';
}
