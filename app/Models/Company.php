<?php

namespace App\Models;

use App\Models\Base;
use DB;

class Company extends Base {

    public function getCompanies($select = null, $order_by = "id", $order_dir = "asc") {
        $db = DB::table($this->tblCompany);
        if ($select) {
            $db->select($select);
        }
        $db->orderBy($order_by, $order_dir);
        return $db->get();
    }

    public function getCompanyById($id) {
        return DB::table($this->tblCompany)->where("id", "=", $id)->limit(1)->get();
    }

    public function addCompany($data) {
        $data["created_at"] = $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblCompany)->insert($data);
    }

    public function updateCompanyById($data, $id) {
        $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblCompany)->where("id", $id)->update($data);
    }

}
