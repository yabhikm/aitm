<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Auth extends Authenticatable {

// 	protected $hidden = ['password'];
    protected $table = 'users';
    public $timestamps = false;

}
