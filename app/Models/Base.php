<?php

namespace App\Models;

use DB;

class Base {

    public $tblCompany = "companies";
    public $tblManfacture = "manufactures";
    public $tblModel = "models";
    public $tblCategory = "categories";
    public $tblLabel = "status_labels";
    public $tblLocation = "locations";
    public $tblDepartment = "departments";
    public $tblCountry = "master_countries";
    public $tblUser = "users";
    public $tblDevice = "devices";
    public $tblDeviceLog = "device_logs";
    public $tblSupplier = "suppliers";
    public $tblPurchase = "purchases";

    public function getCountries() {
        return DB::table($this->tblCountry)->orderBy("name")->get();
    }

}
