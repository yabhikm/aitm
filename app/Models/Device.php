<?php

namespace App\Models;

use App\Models\Base;
use DB;

class Device extends Base {

    public function getDevices() {
        return DB::table($this->tblDevice . ' as d')
               ->join($this->tblModel . ' as mdl', 'd.model_id', '=', 'mdl.id')
               ->join($this->tblManfacture . ' as mft', 'mdl.manufact_id', '=', 'mft.id')
               ->join($this->tblCompany . ' as cmp', 'd.company_id', '=', 'cmp.id')
               ->join($this->tblLocation . ' as loc', 'd.location_id', '=', 'loc.id')
               ->join($this->tblLabel . ' as lbl', 'd.current_status', '=', 'lbl.id')
               ->leftJoin($this->tblUser . ' as u', 'd.user_id', '=', 'u.id')
               ->leftJoin($this->tblDepartment . ' as dpmt', 'd.department_id', '=', 'dpmt.id')
               ->select('d.*', 'mft.name as manufacture', 'mdl.name as model', 'cmp.name as company', 'loc.name as location')
               ->addSelect('lbl.name as label')
               ->addSelect('u.username as user', 'dpmt.name as department')
               ->addSelect(DB::raw('case when lbl.status_type = "Deployed" then true else false end as is_deployed'))
               ->addSelect(DB::raw('case when lbl.status_type = "Deployable" then true else false end as is_deployable'))
               ->get();
    }

    public function getDeviceById($id) {
        return DB::table($this->tblDevice)->where("id", "=", $id)->limit(1)->get();
    }

    public function addDevice($data) {
        $data["created_at"] = $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblDevice)->insertGetId($data);
    }

    public function updateDeviceById($data, $id) {
        $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblDevice)->where("id", $id)->update($data);
    }
    
    public function addDeviceLog($data) {
        $data["logged_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblDeviceLog)->insert($data);
    }

}
