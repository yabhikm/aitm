create table manufactures (
id int unsigned not null PRIMARY KEY AUTO_INCREMENT,
name char(80) not null,
created_at datetime,
updated_at datetime
) ENGINE=MyISAM

create table models (
id int unsigned not null PRIMARY KEY AUTO_INCREMENT,
name varchar(100) not null,
manufact_id int unsigned not null,
category_id int unsigned not null,
model_no varchar(50) default null,
created_at datetime,
updated_at datetime
) ENGINE=MyISAM

create table categories (
	id int unsigned not null PRIMARY KEY AUTO_INCREMENT,
	name char(80) not null,
	category_type enum("Device","Accessory","Component"),
	created_at datetime,
	updated_at datetime
) ENGINE=MyISAM

create table status_labels (
	id int unsigned not null PRIMARY KEY AUTO_INCREMENT,
	name char(80) not null,
	status_type enum("Deployable", "Non Deployable", "Archived", "Deployed", "Pending"),
	created_at datetime,
	updated_at datetime,
	notes text
) ENGINE=MyISAM

create table master_countries (
	id int unsigned not null PRIMARY KEY AUTO_INCREMENT,
	country_code char(3) not null,
	name varchar(50)
) ENGINE=MyISAM

create table locations (
	id int unsigned not null PRIMARY KEY AUTO_INCREMENT,
	name char(80) not null,
        company_id int unsigned not null,
	country char(3) not null,
	city varchar(80) default null,
	state varchar(80) default null,
	street1 varchar(80) default null,
	street2 varchar(80) default null,
	zip varchar(12) default null,
	created_at datetime,
	updated_at datetime
) ENGINE=MyISAM

create table departments (
	id int unsigned not null PRIMARY KEY AUTO_INCREMENT,
	name char(80) not null,
	location_id int unsigned not null,
	created_at datetime,
	updated_at datetime
) ENGINE=MyISAM

create table users (
	id int unsigned not null PRIMARY KEY AUTO_INCREMENT,
	email varchar(255) default null,
	username varchar(80) default null,
	password varchar(255) default null,
	usertype enum("End User", "Admin", "Global Admin", "Super Admin") default null comment "End User, Admin, Global Admin, Super Admin",
	permission_type tinyint default 1 comment "1-User Type Based, 2-Custom Based",
	permission varchar(255) default null,
	firstname varchar(80) default null,
	lastname varchar(80) default null,
	job_title varchar(80) default null,
	mobile char(14) default null,
	emp_code varchar(16) default null,
	activated tinyint default 0 comment "0-Deactivated, 1-Activated",
	reset_password_code char(10) default 0,
	location_id int unsigned default 0,
	company_id int unsigned default 0,
	department_id int unsigned default 0,
	created_at datetime,
	updated_at datetime,
	notes text
) ENGINE=InnoDB;

create table devices (
	id int unsigned not null PRIMARY KEY AUTO_INCREMENT,
	device_tag varchar(120) default null,
	device_name varchar(255) default null,
	model_id int unsigned not null,
	current_status int unsigned,
	serial varchar(255) default null,
	supplier_id int unsigned default 0,
	company_id int unsigned default 0,
	location_id int unsigned default 0,
	assign_to enum("User", "Department") default null,
	user_id int unsigned default 0,
	department_id int unsigned default 0,
	purchase_date date default null,
	purchase_cost decimal(6,2) default null,
	order_number varchar(120) default null,
	created_by int unsigned default 0,
	created_at datetime,
	updated_at datetime,
	deleted_at datetime,
	archived_item tinyint(1) default 0 comment "0-No Archived, 1-Archived",
	warrenty_months int unsigned default 0,
	is_requestable tinyint(1) default 0 comment "0-No, 1-Yes",
	last_checkin datetime default null,
	last_checkout datetime default null,
	expected_checkin datetime default null
)

create table device_logs (
	id int unsigned not null primary key auto_increment,
	device_id int unsigned not null,
	action_type  tinyint not null comment '1-Checkout, 2-Checkin',
	user_id int unsigned default 0,
	department_id int unsigned default 0,
	assign_to enum("User", "Department") default null,
	logged_by int unsigned not null,
	logged_at datetime,
        log_remarks text
);

ALTER TABLE `users` ADD `remember_token` VARCHAR(255) NULL DEFAULT NULL AFTER `updated_at`;
ALTER TABLE `devices` ADD `last_remarks` TEXT NULL ;

CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted_at` datetime NULL DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

create table if not exists `purchases` (
    `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `purchase_date` date not null,
    `invoice_number` varchar(255) default null,
    `company_id` int unsigned not null,
    `supplier_id` int unsigned not null,
    `notes` text default null,
    `documents` text default null,
    `created_at` datetime NULL DEFAULT NULL,
    `updated_at` datetime NULL DEFAULT NULL,
    `created_by` int unsigned default null,
    `updated_by` int unsigned default null
) ENGINE=MyISAM;